# WiFi Control Relay - Rpi3
---
The Raspberry Pi 3 has built-in WiFi hardware making it ideal for network projects like the web-controlled relay that we made with a NodeMCU in the previous article. We can replicate this, using a web framework called Bottle running on the Raspberry Pi.
The first step in doing this is to install the Bottle web framework using the commands:

### `$ sudo apt-get update`
### `$ sudo apt-get install python-bottle`
### `$ sudo python web_relay.py`
---
You need your RPi 3 IP adress, the enter in browser: `<ipadress>/on`

![Scheme](webpage.jpg)

After the initial imports of the Bottle and RPi.GPIO libraries, the control pin for the relay is defined and set to be an output. There then follow three functions, each marked with a line preceding it that starts `@route`. Each of these functions will be called when the web server receives a request for a particular resource. So, `“/”`, the root page will just return the contents of the template `“home.tpl”`.

If the request is for `“/on”` or `“/off”` then the template is still returned, but first the relay is switched on or off as appropriate. The final section of the code is a try/finally block that starts the web server running `ctrl-c` is pressed.